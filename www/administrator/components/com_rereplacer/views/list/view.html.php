<?php
/**
 * List View
 *
 * @package         ReReplacer
 * @version         5.11.3
 *
 * @author          Peter van Westen <peter@nonumber.nl>
 * @link            http://www.nonumber.nl
 * @copyright       Copyright © 2014 NoNumber All Rights Reserved
 * @license         http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * List View
 */
class ReReplacerViewList extends JViewLegacy
{
	protected $enabled;
	protected $list;
	protected $pagination;
	protected $state;
	protected $config;
	protected $parameters;

	/**
	 * Display the view
	 *
	 */
	public function display($tpl = null)
	{
		require_once JPATH_PLUGINS . '/system/nnframework/helpers/parameters.php';
		$this->parameters = NNParameters::getInstance();

		$this->enabled = ReReplacerHelper::isEnabled();
		$this->list = $this->get('Items');
		$this->pagination = $this->get('Pagination');
		$this->state = $this->get('State');
		$this->config = $this->parameters->getComponentParams('rereplacer', $this->state->params);

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		$options = array();
		$options[] = JHtml::_('select.option', '1', JText::_('RR_SHOW_SEARCH_AND_REPLACE_FIELDS'));
		$options[] = JHtml::_('select.option', '0', JText::_('RR_HIDE_SEARCH_AND_REPLACE_FIELDS'));
		$this->f_fields = $options;

		$this->addToolbar();
		$this->sidebar = JHtmlSidebar::render();

		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 */
	protected function addToolbar()
	{
		$state = $this->get('State');
		$canDo = ReReplacerHelper::getActions();

		$viewLayout = JFactory::getApplication()->input->get('layout', 'default');

		if ($viewLayout == 'import')
		{
			// Set document title
			JFactory::getDocument()->setTitle(JText::_('REREPLACER') . ': ' . JText::_('NN_IMPORT_ITEMS'));
			// Set ToolBar title
			JToolbarHelper::title(JText::_('REREPLACER') . ': ' . JText::_('NN_IMPORT_ITEMS'), 'rereplacer icon-nonumber');
			// Set toolbar items for the page
			JToolbarHelper::back();
		}
		else
		{
			// Set document title
			JFactory::getDocument()->setTitle(JText::_('REREPLACER') . ': ' . JText::_('NN_LIST'));
			// Set ToolBar title
			JToolbarHelper::title(JText::_('REREPLACER') . ': ' . JText::_('NN_LIST'), 'rereplacer icon-nonumber');
			// Set toolbar items for the page
			if ($canDo->get('core.create'))
			{
				JToolbarHelper::addNew('item.add');
			}
			if ($canDo->get('core.edit'))
			{
				JToolbarHelper::editList('item.edit');
			}
			if ($canDo->get('core.create'))
			{
				JToolbarHelper::custom('list.copy', 'copy', 'copy', 'JTOOLBAR_DUPLICATE', true);
			}
			if ($canDo->get('core.edit.state') && $state->get('filter.state') != 2)
			{
				JToolbarHelper::publish('list.publish', 'JTOOLBAR_PUBLISH', true);
				JToolbarHelper::unpublish('list.unpublish', 'JTOOLBAR_UNPUBLISH', true);
			}
			if ($canDo->get('core.delete') && $state->get('filter.state') == -2)
			{
				JToolbarHelper::deleteList('', 'list.delete', 'JTOOLBAR_EMPTY_TRASH');
			}
			else if ($canDo->get('core.edit.state'))
			{
				JToolbarHelper::trash('list.trash');
			}
			if ($canDo->get('core.create'))
			{
				JToolbarHelper::custom('list.export', 'box-remove', 'box-remove', 'NN_EXPORT');
				JToolbarHelper::custom('list.import', 'box-add', 'box-add', 'NN_IMPORT', false);
			}
			if ($canDo->get('core.admin'))
			{
				JToolbarHelper::preferences('com_rereplacer');
			}

			JHtmlSidebar::addFilter(
				JText::_('JOPTION_SELECT_PUBLISHED'),
				'filter_state',
				JHtml::_('select.options', JHtml::_('jgrid.publishedOptions', array('archived' => 0)), 'value', 'text', $this->state->get('filter.state'), true)
			);

			$val = $this->state->get('filter.fields') == '' ? $this->config->show_fields : $this->state->get('filter.fields');
			JHtmlSidebar::addFilter(
				'- ' . JText::_('JSELECT') . ' -',
				'filter_fields',
				JHtml::_('select.options', $this->f_fields, 'value', 'text', $val)
			);
		}
	}

	function maxlen($string = '', $maxlen = 60)
	{
		if (JString::strlen($string) > $maxlen)
		{
			$string = JString::substr($string, 0, $maxlen - 3) . '...';
		}
		return $string;
	}

	/**
	 * Returns an array of fields the table can be sorted by
	 *
	 * @return  array  Array containing the field name to sort by as the key and display text as value
	 *
	 * @since   3.0
	 */
	protected function getSortFields()
	{
		return array(
			'a.ordering' => JText::_('JGRID_HEADING_ORDERING'),
			'a.published' => JText::_('JSTATUS'),
			'a.name' => JText::_('JGLOBAL_TITLE'),
			'a.description' => JText::_('JGLOBAL_DESCRIPTION'),
			'a.search' => JText::_('RR_SEARCH'),
			'a.replace' => JText::_('RR_REPLACE'),
			'a.id' => JText::_('JGRID_HEADING_ID')
		);
	}
}
